/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
  roomListURL: 'https://ceas.wmich.edu/caeweb/allroomlist',
  roomScheduleURL:  'https://ceas.wmich.edu/caeweb/roomschedule/api/kiosk',
  // Application Constructor
  initialize: function() {
    this.bindEvents();
  },

  // Bind Event Listeners
  //
  // Bind any events that are required on startup. Common events are:
  // 'load', 'deviceready', 'offline', and 'online'.
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },

  // deviceready Event Handler
  //
  // The scope of 'this' is the event. In order to call the 'receivedEvent'
  // function, we must explicitly call 'app.receivedEvent(...);'
  onDeviceReady: function() {
    app.receivedEvent('deviceready');
  },

  // Update DOM on a Received Event
  receivedEvent: function(id) {
    console.log('Received Event: ' + id);
    switch (id) {
      case 'deviceready':
        app.populateRoomDropDown(app.createCalendar);
        break;
      case 'roomchange':
        app.refreshCalendar();
        break;
      default:
        break;
    }
  },

  populateRoomDropDown: function(callback) {
    const $roomDropDown = $('select.roomDropDown');
    $roomDropDown.empty();
    $roomDropDown.change(function () {
      app.receivedEvent('roomchange');
    });
    $.ajax({
      url: app.roomListURL,
      dataType: "json",
    }).done(function (data) {
      $.each(data, function(i, room) {
        $roomDropDown.append(
            '<option value="' + room.name + '">' + room.name + '</option>'
            );
      });
      callback();
    });
  },

  getRoom: function() {
    const $roomDropDown = $('select.roomDropDown');
    var room = $roomDropDown.val();
    return room;
  },

  createCalendar: function() {
    $('#calendar').fullCalendar({ 
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'agendaWeek,agendaDay'
      }, 
      height: 'auto',
      theme: 'true',
      weekends: false, 
      startParam: 'startdate',
      endParam: 'enddate',
      allDaySlot: false,
      minTime: "08:00:00",
      maxTime: "22:00:00",
      defaultView: 'agendaWeek',
      editable: false, 
      events: {
        url: app.roomScheduleURL,
        dataType: 'json',
        data: {
          room: app.getRoom,
        },
        eventDataTransform: function(eventD) {
          start = new Date(eventD.start.replace(' ', 'T'));
          end = new Date(eventD.end.replace(' ', 'T'));
          start.setHours(start.getHours() + 4);
          end.setHours(end.getHours() + 4);
          eventD.start = start.toISOString();
          eventD.end = end.toISOString();
          return eventD;
        },
      }
    });
  }, 

  refreshCalendar: function() {
    const $calendar = $('#calendar');
    $calendar.fullCalendar('refetchEvents');
  },

};
